/*
 *  Copyright (c) Getswish AB and its affiliates.
 *
 *  This source code is licensed under the MIT license found in the
 *  LICENSE file in the root directory of this source tree.
 */

package se.swish.merchantdemo

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import se.swish.merchantdemo.network.MerchantApiServices

private const val PREFS_NAME = "merchant_prefs"
private const val PREFS_BASE_URL = "base_url"

class MerchantDemoApplication : Application() {

    private lateinit var apiServices: MerchantApiServices
    private lateinit var prefs: SharedPreferences

    override fun onCreate() {
        super.onCreate()

        prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        apiServices = MerchantApiServices(baseUrl)
    }

    fun getMerchantApiServices() = apiServices

    var baseUrl: String
        get() = prefs.getString(PREFS_BASE_URL, "http://your-base-url:3000/").orEmpty()
        set(value) {
            prefs.edit()
                .putString(PREFS_BASE_URL, value)
                .apply()

            apiServices = MerchantApiServices(value)
        }
}
