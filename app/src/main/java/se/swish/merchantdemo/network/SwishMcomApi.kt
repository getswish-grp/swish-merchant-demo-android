/*
 *  Copyright (c) Getswish AB and its affiliates.
 *
 *  This source code is licensed under the MIT license found in the
 *  LICENSE file in the root directory of this source tree.
 */

package se.swish.merchantdemo.network

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path


// NOTE: This is an example of an api that the mobile app and the merchant backend could be using
//       This api is implemented by the sample web app we also provide
interface SwishMcomApi {

    @POST("paymentrequests")
    fun createPaymentRequest(@Body body: CreatePaymentRequestBody): Call<CreatePaymentRequestResponse>

    @GET("paymentrequests/{id}")
    fun getPaymentRequestStatus(@Path("id") id: String): Call<PaymentStatusResponse>

    @POST("refunds")
    fun createRefund(@Body body: CreateRefundBody): Call<CreateRefundResponse>

    @GET("refunds/{id}")
    fun getRefundStatus(@Path("id") id: String): Call<RefundStatusResponse>
}

// For creating a payment request and fetching payment status
data class CreatePaymentRequestBody(val amount: String, val message: String)
data class CreatePaymentRequestResponse(val id: String, val token: String)
data class PaymentStatusResponse(val id: String, val status: String, val paymentReference: String)

// For creating a refund and fetching refund status
data class CreateRefundBody(val originalPaymentReference: String, val amount: String, val message: String)
data class CreateRefundResponse(val id: String, val status: String, val originalPaymentReference: String)
data class RefundStatusResponse(val id: String, val status: String, val originalPaymentReference: String)


