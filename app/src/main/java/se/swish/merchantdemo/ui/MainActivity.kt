/*
 *  Copyright (c) Getswish AB and its affiliates.
 *
 *  This source code is licensed under the MIT license found in the
 *  LICENSE file in the root directory of this source tree.
 */

package se.swish.merchantdemo.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import se.swish.merchantdemo.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, StartFragment())
            .commit()
    }
}
