/*
 *  Copyright (c) Getswish AB and its affiliates.
 *
 *  This source code is licensed under the MIT license found in the
 *  LICENSE file in the root directory of this source tree.
 */

package se.swish.merchantdemo.ui

import androidx.fragment.app.Fragment
import se.swish.merchantdemo.MerchantDemoApplication

fun Fragment.merchantDemoApplication(): MerchantDemoApplication = requireContext().applicationContext as MerchantDemoApplication
