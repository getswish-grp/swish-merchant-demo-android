/*
 *  Copyright (c) Getswish AB and its affiliates.
 *
 *  This source code is licensed under the MIT license found in the
 *  LICENSE file in the root directory of this source tree.
 */

package se.swish.merchantdemo.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_start.*
import se.swish.merchantdemo.R
import java.net.URI


class StartFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_start, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mCommerceButton.setOnClickListener {
            requireFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, MCommerceFragment())
                .addToBackStack(null)
                .commit()
        }

        baseUrlText.setText(merchantDemoApplication().baseUrl)

        saveButton.setOnClickListener {
            val newBaseUrl = baseUrlText.text.toString()
            if (!isValidURI(newBaseUrl)) {
                Toast.makeText(requireContext(), "Check format of URL", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            merchantDemoApplication().baseUrl = newBaseUrl
            Toast.makeText(requireContext(), "Base URL saved!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun isValidURI(uriStr: String): Boolean =
        try {
            val uri = URI(uriStr)
            uri.scheme == "http" || uri.scheme == "https"
        } catch (e: Exception) {
            false
        }
}
