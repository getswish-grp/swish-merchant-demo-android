/*
 *  Copyright (c) Getswish AB and its affiliates.
 *
 *  This source code is licensed under the MIT license found in the
 *  LICENSE file in the root directory of this source tree.
 */

package se.swish.merchantdemo.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_mcommerce.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import se.swish.merchantdemo.R
import se.swish.merchantdemo.network.*


class MCommerceFragment : Fragment() {

    private var lastPaymentId: String? = null
    private var lastPaymentReference: String? = null
    private var lastRefundId: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_mcommerce, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startPaymentButton.setOnClickListener {
            startPayment()
        }

        updateStatusButton.setOnClickListener {
            updateStatus()
        }

        startRefundButton.setOnClickListener {
            startRefund()
        }

        updateRefundStatusButton.setOnClickListener {
            updateRefundStatus()
        }
    }

    private fun startSwish(token: String) {
        if (!checkIsSwishInstalled()) {
            Toast.makeText(requireContext(), "Swish is not installed", Toast.LENGTH_LONG).show()
            return
        }

        val url = Uri.Builder()
            .scheme("swish")
            .authority("paymentrequest")
            .appendQueryParameter("token", token)
            .build()

        val intent = Intent(Intent.ACTION_VIEW, url).apply {
            setPackage("se.bankgirot.swish")
        }

        startActivity(intent)
    }

    private fun startPayment() {
        if (amountEditText.text.toString().isBlank()) {
            statusText.text = "Amount must not be empty"
            return
        }

        val body = CreatePaymentRequestBody(amountEditText.text.toString(), messageEditText.text.toString())
        merchantDemoApplication().getMerchantApiServices().mcomApi.createPaymentRequest(body)
            .enqueue(object : Callback<CreatePaymentRequestResponse> {

                override fun onResponse(call: Call<CreatePaymentRequestResponse>, response: Response<CreatePaymentRequestResponse>) {
                    val responseBody = response.body()
                    if (response.isSuccessful && responseBody != null) {
                        lastPaymentId = responseBody.id
                        startSwish(responseBody.token)
                        statusText.text = "Created request. \nid: ${responseBody.id}\ntoken: ${responseBody.token}"
                    } else {
                        statusText.text = "Error reading response"
                    }
                }

                override fun onFailure(call: Call<CreatePaymentRequestResponse>, t: Throwable) {
                    t.printStackTrace()
                    statusText.text = "Invalid response from backend: ${t.message}"
                }
            })
    }

    private fun updateStatus() {
        if (lastPaymentId == null) {
            Toast.makeText(requireContext(), "There is no ongoing payment request, start one first", Toast.LENGTH_LONG)
                .show()
            return
        }

        merchantDemoApplication().getMerchantApiServices().mcomApi.getPaymentRequestStatus(lastPaymentId.orEmpty())
            .enqueue(object : Callback<PaymentStatusResponse> {

                override fun onResponse(call: Call<PaymentStatusResponse>, response: Response<PaymentStatusResponse>) {
                    val responseBody = response.body()
                    if (response.isSuccessful && responseBody != null) {
                        statusText.text = if (responseBody.status == "PAID") {
                            lastPaymentReference = responseBody.paymentReference
                            "${responseBody.status} for payment with reference: ${responseBody.paymentReference}"
                        } else {
                            "${responseBody.status} for payment with id: ${responseBody.id}"
                        }
                    } else {
                        statusText.text = "Request Confirmation Error"
                    }
                }

                override fun onFailure(call: Call<PaymentStatusResponse>, t: Throwable) {
                    t.printStackTrace()
                    statusText.text = "Invalid response from backend: ${t.message}"
                }
            })
    }

    private fun startRefund() {
        if (amountEditText.text.toString().isBlank()) {
            statusText.text = "Amount must not be empty"
            return
        }

        if (lastPaymentReference == null) {
            statusText.text = "No Payment Reference found"
            return
        }

        val body = CreateRefundBody(lastPaymentReference!!, amountEditText.text.toString(), messageEditText.text.toString())
        merchantDemoApplication().getMerchantApiServices().mcomApi.createRefund(body)
            .enqueue(object : Callback<CreateRefundResponse> {

                override fun onResponse(call: Call<CreateRefundResponse>, response: Response<CreateRefundResponse>) {
                    val responseBody = response.body()

                    if (response.isSuccessful && responseBody != null) {
                        lastRefundId = responseBody.id
                        statusText.text = "${responseBody.status} for refund with paymentreference: ${responseBody.originalPaymentReference}"
                    } else {
                        statusText.text = "Error reading response"
                    }
                }

                override fun onFailure(call: Call<CreateRefundResponse>, t: Throwable) {
                    t.printStackTrace()
                    statusText.text = "Invalid response from backend: ${t.message}"
                }
            })
    }

    private fun updateRefundStatus() {
        if (lastRefundId == null) {
            Toast.makeText(requireContext(), "There is no ongoing refund, start one first", Toast.LENGTH_LONG).show()
            return
        }

        merchantDemoApplication().getMerchantApiServices().mcomApi.getRefundStatus(lastRefundId!!)
            .enqueue(object : Callback<RefundStatusResponse> {

                override fun onResponse(call: Call<RefundStatusResponse>, response: Response<RefundStatusResponse>) {
                    val responseBody = response.body()

                    if (response.isSuccessful && responseBody != null) {
                        lastRefundId = responseBody.id
                        lastPaymentReference = responseBody.originalPaymentReference

                        if (responseBody.status == "PAID") {
                            statusText.text = "${responseBody.status} for refund with reference: ${responseBody.originalPaymentReference}"
                        } else {
                            statusText.text = "${responseBody.status} for refund with id: ${responseBody.id}"
                        }
                    } else {
                        statusText.text = "Refund Confirmation Error"
                    }
                }

                override fun onFailure(call: Call<RefundStatusResponse>, t: Throwable) {
                    t.printStackTrace()
                    statusText.text = "Invalid response from backend: ${t.message}"
                }
            })
    }

    private fun checkIsSwishInstalled() =
        try {
            requireContext().packageManager.getPackageInfo("se.bankgirot.swish", 0)
            true
        } catch (e: Exception) {
            false
        }
}
