# Swish Merchant Demo Android

## About ##
This project demonstrates how to integrate Swish in an Android app as a merchant. The demo app targets a local Swish Merchant Demo Web installation by default. You can run the Swish Merchant Demo Web on a local machine to test the API calls.

## Examples ##

### Checking if the Swish app is installed

```java
public static boolean isSwishInstalled(Context context) {
    try {
        context.getPackageManager()
                .getPackageInfo("se.bankgirot.swish", 0);
        return true;
    } catch (PackageManager.NameNotFoundException e) {
        // Swish app is not installed
        return false;
    }
}
```

### Switching to the Swish app

```java
public static boolean openSwishWithToken(Context context, String token, String callBackUrl) {
    if ( token == null
            || token.length() == 0
            || callBackUrl == null
            || callBackUrl.length() == 0
            || context == null) {
        return false;
    }

    // Construct the uri
    // Note that appendQueryParameter takes care of uri encoding
    // the parameters
    Uri url = new Uri.Builder()
            .scheme("swish")
            .authority("paymentrequest")
            .appendQueryParameter("token", token)
            .appendQueryParameter("callbackurl", callBackUrl)
            .build();

    Intent intent = new Intent(Intent.ACTION_VIEW, url);
    intent.setPackage("se.bankgirot.swish");

    try {
        context.startActivity(intent);
    } catch (Exception e){
        // Unable to start Swish
        return false;
    }

    return true;
}
```
